package main

import (
	"fmt"
	"math"
)

func main() {
	var numberTask string
	fmt.Print("Введите номер задачи: ")
	fmt.Scanln(&numberTask)
	switch numberTask {
	case "1":
		var a, b int
		fmt.Print("Введите сторону a: ")
		fmt.Scanln(&a)
		fmt.Print("Введите сторону b: ")
		fmt.Scanln(&b)
		fmt.Printf("Площадь прямоугольника: %v\n", a*b)
	case "2":
		var square float64
		fmt.Print("Введите площадь окружности: ")
		fmt.Scanln(&square)
		r := math.Sqrt(square / math.Pi)
		D := r * 2
		L := math.Pi * D
		fmt.Printf("Диаметр окружности: %5.0f\n", D)
		fmt.Printf("Длина окружности: %5.0f\n", L)
	case "3":
		var number int
		fmt.Print("Введите трехзначное число: ")
		fmt.Scanln(&number)
		a := number / 100
		b := number % 100 / 10
		c := number % 10
		fmt.Println("Сотни: ", a)
		fmt.Println("Десятки: ", b)
		fmt.Println("Единицы: ", c)
	default:
		fmt.Println("Вы не ввели номер задачи. Завершение программы")
	}

}
